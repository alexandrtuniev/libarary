define(['angular', 'angular-route', 'controllers', 'services'], function () {
    var app = angular.module('app', ['ngRoute', 'appControllers', 'appServices']);
    app.config(['$routeProvider',
      function ($routeProvider) {
          $routeProvider.
            when('/home', {
                templateUrl: 'templates/home.html',
                controller: 'homeCtrl'
            }).
            when('/user', {
                templateUrl: 'templates/user.html',
                controller: 'userCtrl'
            }).
            when('/login', {
                templateUrl: 'templates/login.html',
                controller: 'loginCtrl'
            }).
            when('/category/:genre', {
                templateUrl: 'templates/category.html',
                controller: 'categoryCtrl'
            }).
            when('/book/:bookID', {
                templateUrl: 'templates/bookDetails.html',
                controller: 'bookDetailsCtrl'
            }).
            when('/settings', {
                templateUrl: 'templates/settings.html',
                controller: 'settingsCtrl'
            }).
            when('/profile', {
                templateUrl: 'templates/settings.html',
                controller: 'settingsCtrl'

            }).                    
            when('/help', {
                templateUrl: 'templates/settings.html',
                controller: 'settingsCtrl'
            }).
             when('/admin', {
                 templateUrl: 'templates/admin.html',
                 controller: 'adminCtrl'
             }).
            otherwise({
                redirectTo: '/home'
            });
      }]);
    angular.bootstrap(document, ['app']);
});
    