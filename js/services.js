define(['angular', 'angular-resource'], function (angular,ngResource) {
   var appServices = angular.module('appServices', ['ngResource']);
   appServices.factory('Categories', ['$resource', function ($resource) {
       return $resource('data/categories.json', {}, { query: { method: 'GET', isArray: true } });
   }]);
   appServices.factory('Books', ['$resource', function ($resource) {
       return $resource('data/books.json', {}, { query: { method: 'GET', isArray: true } });
   }]);
   
});

