define(['angular'], function (angular) {

    var appControllers = angular.module('appControllers', []);

    appControllers.controller('homeCtrl', ['$scope', 'Categories', function ($scope, Categories) {
        $scope.categories = Categories.query();
    }]);
    appControllers.controller('userCtrl', function ($scope) {
        $scope.welcomeText = "hello this is User page!";
    });

    appControllers.controller('loginCtrl', function ($scope) {
        $scope.welcomeText = "hello this is login page!";
    });

    appControllers.controller('categoryCtrl', function ($scope, Books, $routeParams) {
        $scope.genre = $routeParams.genre;
        $scope.books = Books.query();
    });
    appControllers.controller('bookDetailsCtrl', function ($scope, Books, $routeParams) {
        $scope.book = Books.query();
        $scope.id = $routeParams.bookID;
    });
    appControllers.controller('CategoryListCtrl', ['Categories', function (Categories) {
        var self = this;
        self.categories = Categories.query();
    }]);
    appControllers.controller('adminCtrl', function (Categories,Books) {
        self.categories = Categories.query();
        self.books = Books.query();
    });





    appControllers.controller('settingsCtrl', function ($http) {
    });
});


