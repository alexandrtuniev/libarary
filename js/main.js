requirejs.config({
    baseURL: '/js',
    paths: {
        // Libs
        'angular': 'lib/angular',
        'angular-route': 'lib/angular-route.min',
        'angular-resource':'lib/angular-resource',
        'bootstrap': 'lib/bootstrap',
        'jquery': 'lib/jquery-1.11.1',
        // app
        'app': 'app',
        'controllers': 'controllers',
        'services': 'services'
    },
    shim: {
        'angular': {
            exports: 'angular'
        },
        'angular-route': {
            deps: ['angular']
        },
        'angular-resource':{
            deps: ['angular']
        },
        'jquery': {
            exports: 'jQuery'
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'services': {
            deps: ['angular-resource']
        }
    }
});

// Start the main app logic.
requirejs(['jquery','app', 'bootstrap'],
function ($) {
    $(document).ready(function () {
        $('[data-toggle=offcanvas]').click(function () {
            $('.row-offcanvas').toggleClass('active')
        });
    });
});